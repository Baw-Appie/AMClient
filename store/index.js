export const state = () => {
  return {
    ip: null,
    pass: null
  }
}

export const mutations = {
  SET_IP(state, data) {
    state.ip = data || null
  },
  SET_PASS(state, data) {
    state.pass = data || null
  }
}

export const getters = {
  getPass(state) {
    return state.pass
  },
  getIP(state) {
    return state.ip
  }
}
